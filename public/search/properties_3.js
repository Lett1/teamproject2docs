var searchData=
[
  ['defaultcolor',['DefaultColor',['../class_y_a_l_s___wasp_edition_1_1_global_config_1_1_global_config_settings.html#ab8b23318e9f0a8a1fd750bbbd54ed92b',1,'YALS_WaspEdition::GlobalConfig::GlobalConfigSettings']]],
  ['description',['Description',['../class_components_1_1_components_1_1_component.html#a424a9c4a3c3f5cb41dbf11c6ddd05af3',1,'Components.Components.Component.Description()'],['../interface_shared_1_1_i_node.html#a1194163e5fb1e5b2687b51da41d85812',1,'Shared.INode.Description()'],['../class_y_a_l_s___wasp_edition_1_1_view_models_1_1_node_v_m.html#ae0825a4efaa7d0b1909ea8dcd120fde8',1,'YALS_WaspEdition.ViewModels.NodeVM.Description()']]],
  ['disconnectcommand',['DisconnectCommand',['../class_y_a_l_s___wasp_edition_1_1_view_models_1_1_connection_v_m.html#a1530de95bc486fb839d926a7aecee77e',1,'YALS_WaspEdition::ViewModels::ConnectionVM']]]
];
