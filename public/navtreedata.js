var NAVTREE =
[
  [ "YALS", "index.html", [
    [ "Teamprojekt 2", "md__mnt_c_Users_Lett1_TeamProject2_Organisatorisches_Aufgaben_Aufteilung_Team_4.html", null ],
    [ "TeamProject2", "md__mnt_c_Users_Lett1_TeamProject2_README.html", null ],
    [ "NUnit 3.11 - October 11, 2018", "md__mnt_c_Users_Lett1_TeamProject2_YALS_packages_NUnit.3.11.0_CHANGES.html", null ],
    [ "Packages", null, [
      [ "Packages", "namespaces.html", "namespaces" ],
      [ "Package Functions", "namespacemembers.html", [
        [ "All", "namespacemembers.html", null ],
        [ "Enumerations", "namespacemembers_enum.html", null ]
      ] ]
    ] ],
    [ "Classes", "annotated.html", [
      [ "Class List", "annotated.html", "annotated_dup" ],
      [ "Class Index", "classes.html", null ],
      [ "Class Hierarchy", "hierarchy.html", "hierarchy" ],
      [ "Class Members", "functions.html", [
        [ "All", "functions.html", "functions_dup" ],
        [ "Functions", "functions_func.html", null ],
        [ "Properties", "functions_prop.html", null ],
        [ "Events", "functions_evnt.html", null ]
      ] ]
    ] ],
    [ "Files", null, [
      [ "File List", "files.html", "files" ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"_app_8xaml_8cs_source.html",
"class_y_a_l_s___wasp_edition_1_1_model_1_1_component_1_1_connection_1_1_connection.html#af1e4c63bd35b082433517a15d1f9f6a8",
"interface_y_a_l_s___wasp_edition_1_1_model_1_1_component_1_1_manager_1_1_i_component_manager.html#a400da084f319f9aabe89436a1f5c1449"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';