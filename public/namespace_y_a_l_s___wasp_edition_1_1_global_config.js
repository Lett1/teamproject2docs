var namespace_y_a_l_s___wasp_edition_1_1_global_config =
[
    [ "ColorConfigurationWindow", "class_y_a_l_s___wasp_edition_1_1_global_config_1_1_color_configuration_window.html", "class_y_a_l_s___wasp_edition_1_1_global_config_1_1_color_configuration_window" ],
    [ "ColorSettingsListBox", "class_y_a_l_s___wasp_edition_1_1_global_config_1_1_color_settings_list_box.html", "class_y_a_l_s___wasp_edition_1_1_global_config_1_1_color_settings_list_box" ],
    [ "GetColorWithGlobalConfigSettings", "class_y_a_l_s___wasp_edition_1_1_global_config_1_1_get_color_with_global_config_settings.html", "class_y_a_l_s___wasp_edition_1_1_global_config_1_1_get_color_with_global_config_settings" ],
    [ "GlobalConfigSettings", "class_y_a_l_s___wasp_edition_1_1_global_config_1_1_global_config_settings.html", "class_y_a_l_s___wasp_edition_1_1_global_config_1_1_global_config_settings" ],
    [ "IGetColorForPin", "interface_y_a_l_s___wasp_edition_1_1_global_config_1_1_i_get_color_for_pin.html", "interface_y_a_l_s___wasp_edition_1_1_global_config_1_1_i_get_color_for_pin" ],
    [ "SerializableColor", "class_y_a_l_s___wasp_edition_1_1_global_config_1_1_serializable_color.html", "class_y_a_l_s___wasp_edition_1_1_global_config_1_1_serializable_color" ],
    [ "ValueInputWindow", "class_y_a_l_s___wasp_edition_1_1_global_config_1_1_value_input_window.html", "class_y_a_l_s___wasp_edition_1_1_global_config_1_1_value_input_window" ]
];