var interface_y_a_l_s___wasp_edition_1_1_model_1_1_component_1_1_connection_1_1_i_connection =
[
    [ "AddInputPin", "interface_y_a_l_s___wasp_edition_1_1_model_1_1_component_1_1_connection_1_1_i_connection.html#afdba1098d4d27937a557450af3327c58", null ],
    [ "RemoveInputPin", "interface_y_a_l_s___wasp_edition_1_1_model_1_1_component_1_1_connection_1_1_i_connection.html#a98503c3c2eb10ab2cee1322dccd82199", null ],
    [ "InputPins", "interface_y_a_l_s___wasp_edition_1_1_model_1_1_component_1_1_connection_1_1_i_connection.html#a030edad1d7e559bc14801b5cb9c7724e", null ],
    [ "Output", "interface_y_a_l_s___wasp_edition_1_1_model_1_1_component_1_1_connection_1_1_i_connection.html#a673f8ae160b3274a4fa5c47146063fcb", null ]
];