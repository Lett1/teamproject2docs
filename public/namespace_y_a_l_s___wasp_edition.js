var namespace_y_a_l_s___wasp_edition =
[
    [ "Commands", "namespace_y_a_l_s___wasp_edition_1_1_commands.html", "namespace_y_a_l_s___wasp_edition_1_1_commands" ],
    [ "Converters", "namespace_y_a_l_s___wasp_edition_1_1_converters.html", "namespace_y_a_l_s___wasp_edition_1_1_converters" ],
    [ "DI", "namespace_y_a_l_s___wasp_edition_1_1_d_i.html", "namespace_y_a_l_s___wasp_edition_1_1_d_i" ],
    [ "GlobalConfig", "namespace_y_a_l_s___wasp_edition_1_1_global_config.html", "namespace_y_a_l_s___wasp_edition_1_1_global_config" ],
    [ "Model", "namespace_y_a_l_s___wasp_edition_1_1_model.html", "namespace_y_a_l_s___wasp_edition_1_1_model" ],
    [ "MyEventArgs", "namespace_y_a_l_s___wasp_edition_1_1_my_event_args.html", "namespace_y_a_l_s___wasp_edition_1_1_my_event_args" ],
    [ "ViewModels", "namespace_y_a_l_s___wasp_edition_1_1_view_models.html", "namespace_y_a_l_s___wasp_edition_1_1_view_models" ],
    [ "Views", "namespace_y_a_l_s___wasp_edition_1_1_views.html", "namespace_y_a_l_s___wasp_edition_1_1_views" ],
    [ "App", "class_y_a_l_s___wasp_edition_1_1_app.html", null ],
    [ "MainWindow", "class_y_a_l_s___wasp_edition_1_1_main_window.html", "class_y_a_l_s___wasp_edition_1_1_main_window" ]
];