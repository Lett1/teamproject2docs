var hierarchy =
[
    [ "Application", null, [
      [ "YALS_WaspEdition.App", "class_y_a_l_s___wasp_edition_1_1_app.html", null ]
    ] ],
    [ "ComponentTest.ComponentTests", "class_component_test_1_1_component_tests.html", null ],
    [ "YALS_WaspEdition.ViewModels.ConnectionVM", "class_y_a_l_s___wasp_edition_1_1_view_models_1_1_connection_v_m.html", null ],
    [ "YALS_WaspEdition.Model.Serialization.CurrentState", "class_y_a_l_s___wasp_edition_1_1_model_1_1_serialization_1_1_current_state.html", null ],
    [ "EventArgs", null, [
      [ "YALS_WaspEdition.MyEventArgs.PinSelectedEventArgs", "class_y_a_l_s___wasp_edition_1_1_my_event_args_1_1_pin_selected_event_args.html", null ]
    ] ],
    [ "YALS_WaspEdition.GlobalConfig.GlobalConfigSettings", "class_y_a_l_s___wasp_edition_1_1_global_config_1_1_global_config_settings.html", null ],
    [ "ICommand", null, [
      [ "YALS_WaspEdition.Commands.Command", "class_y_a_l_s___wasp_edition_1_1_commands_1_1_command.html", null ]
    ] ],
    [ "YALS_WaspEdition.Model.Reflection.IComponentLoader", "interface_y_a_l_s___wasp_edition_1_1_model_1_1_reflection_1_1_i_component_loader.html", [
      [ "YALS_WaspEdition.Model.Component.Reflection.ComponentLoader", "class_y_a_l_s___wasp_edition_1_1_model_1_1_component_1_1_reflection_1_1_component_loader.html", null ]
    ] ],
    [ "ComponentTest.Model.Reflection.IComponentLoader", "interface_component_test_1_1_model_1_1_reflection_1_1_i_component_loader.html", [
      [ "ComponentTest.Model.Reflection.ComponentLoader", "class_component_test_1_1_model_1_1_reflection_1_1_component_loader.html", null ]
    ] ],
    [ "YALS_WaspEdition.Model.Reflection.IComponentLoaderController", "interface_y_a_l_s___wasp_edition_1_1_model_1_1_reflection_1_1_i_component_loader_controller.html", [
      [ "YALS_WaspEdition.Model.Component.Reflection.ComponentLoaderController", "class_y_a_l_s___wasp_edition_1_1_model_1_1_component_1_1_reflection_1_1_component_loader_controller.html", null ]
    ] ],
    [ "ComponentTest.Model.Reflection.IComponentLoaderController", "interface_component_test_1_1_model_1_1_reflection_1_1_i_component_loader_controller.html", [
      [ "ComponentTest.Model.Reflection.ComponentLoaderController", "class_component_test_1_1_model_1_1_reflection_1_1_component_loader_controller.html", null ]
    ] ],
    [ "ComponentTest.Model.Component.Manager.IComponentManager", "interface_component_test_1_1_model_1_1_component_1_1_manager_1_1_i_component_manager.html", [
      [ "ComponentTest.Model.Component.Manager.ComponentManager", "class_component_test_1_1_model_1_1_component_1_1_manager_1_1_component_manager.html", null ]
    ] ],
    [ "YALS_WaspEdition.Model.Component.Manager.IComponentManager", "interface_y_a_l_s___wasp_edition_1_1_model_1_1_component_1_1_manager_1_1_i_component_manager.html", [
      [ "YALS_WaspEdition.Model.Component.Manager.ComponentManager", "class_y_a_l_s___wasp_edition_1_1_model_1_1_component_1_1_manager_1_1_component_manager.html", null ]
    ] ],
    [ "ComponentTest.Model.Connection.IConnection", "interface_component_test_1_1_model_1_1_connection_1_1_i_connection.html", [
      [ "ComponentTest.Model.Connection.Connection", "class_component_test_1_1_model_1_1_connection_1_1_connection.html", null ]
    ] ],
    [ "YALS_WaspEdition.Model.Component.Connection.IConnection", "interface_y_a_l_s___wasp_edition_1_1_model_1_1_component_1_1_connection_1_1_i_connection.html", [
      [ "YALS_WaspEdition.Model.Component.Connection.Connection", "class_y_a_l_s___wasp_edition_1_1_model_1_1_component_1_1_connection_1_1_connection.html", null ]
    ] ],
    [ "YALS_WaspEdition.Model.Component.Connection.IConnectionManager", "interface_y_a_l_s___wasp_edition_1_1_model_1_1_component_1_1_connection_1_1_i_connection_manager.html", [
      [ "YALS_WaspEdition.Model.Component.Connection.ConnectionManager", "class_y_a_l_s___wasp_edition_1_1_model_1_1_component_1_1_connection_1_1_connection_manager.html", null ]
    ] ],
    [ "ComponentTest.Model.Connection.IConnectionManager", "interface_component_test_1_1_model_1_1_connection_1_1_i_connection_manager.html", [
      [ "ComponentTest.Model.Connection.ConnectionManager", "class_component_test_1_1_model_1_1_connection_1_1_connection_manager.html", null ]
    ] ],
    [ "YALS_WaspEdition.Model.Serialization.ICurrentStateSerializer", "interface_y_a_l_s___wasp_edition_1_1_model_1_1_serialization_1_1_i_current_state_serializer.html", [
      [ "YALS_WaspEdition.Model.Serialization.BinaryCurrentStateSerializer", "class_y_a_l_s___wasp_edition_1_1_model_1_1_serialization_1_1_binary_current_state_serializer.html", null ]
    ] ],
    [ "Shared.IDisplayable", "interface_shared_1_1_i_displayable.html", [
      [ "Shared.IDisplayableNode", "interface_shared_1_1_i_displayable_node.html", [
        [ "Components.Components.Component", "class_components_1_1_components_1_1_component.html", [
          [ "Components.Components.AndComponent", "class_components_1_1_components_1_1_and_component.html", null ],
          [ "Components.Components.BoolToIntComponent", "class_components_1_1_components_1_1_bool_to_int_component.html", null ],
          [ "Components.Components.ConstantComponent", "class_components_1_1_components_1_1_constant_component.html", null ],
          [ "Components.Components.HexDisplayComponent", "class_components_1_1_components_1_1_hex_display_component.html", null ],
          [ "Components.Components.LEDComponent", "class_components_1_1_components_1_1_l_e_d_component.html", null ],
          [ "Components.Components.NotComponent", "class_components_1_1_components_1_1_not_component.html", null ],
          [ "Components.Components.OrComponent", "class_components_1_1_components_1_1_or_component.html", null ],
          [ "Components.Components.SwitchComponent", "class_components_1_1_components_1_1_switch_component.html", null ],
          [ "Components.Components.XORComponent", "class_components_1_1_components_1_1_x_o_r_component.html", null ]
        ] ],
        [ "ComponentTest.Model.Component.Component", "class_component_test_1_1_model_1_1_component_1_1_component.html", [
          [ "ComponentTest.Model.Component.AndComponent", "class_component_test_1_1_model_1_1_component_1_1_and_component.html", null ],
          [ "ComponentTest.Model.Component.OrComponent", "class_component_test_1_1_model_1_1_component_1_1_or_component.html", null ],
          [ "ComponentTest.Model.Component.XORComponent", "class_component_test_1_1_model_1_1_component_1_1_x_o_r_component.html", null ]
        ] ]
      ] ]
    ] ],
    [ "YALS_WaspEdition.GlobalConfig.IGetColorForPin", "interface_y_a_l_s___wasp_edition_1_1_global_config_1_1_i_get_color_for_pin.html", [
      [ "YALS_WaspEdition.GlobalConfig.GetColorWithGlobalConfigSettings", "class_y_a_l_s___wasp_edition_1_1_global_config_1_1_get_color_with_global_config_settings.html", null ]
    ] ],
    [ "IMultiValueConverter", null, [
      [ "YALS_WaspEdition.Converters.BezierConverter", "class_y_a_l_s___wasp_edition_1_1_converters_1_1_bezier_converter.html", null ]
    ] ],
    [ "Shared.INode", "interface_shared_1_1_i_node.html", [
      [ "Shared.IDisplayableNode", "interface_shared_1_1_i_displayable_node.html", null ]
    ] ],
    [ "INotifyPropertyChanged", null, [
      [ "YALS_WaspEdition.ViewModels.ComponentManagerVM", "class_y_a_l_s___wasp_edition_1_1_view_models_1_1_component_manager_v_m.html", null ],
      [ "YALS_WaspEdition.ViewModels.MainVM", "class_y_a_l_s___wasp_edition_1_1_view_models_1_1_main_v_m.html", null ],
      [ "YALS_WaspEdition.ViewModels.NodeVM", "class_y_a_l_s___wasp_edition_1_1_view_models_1_1_node_v_m.html", null ],
      [ "YALS_WaspEdition.ViewModels.PinVM", "class_y_a_l_s___wasp_edition_1_1_view_models_1_1_pin_v_m.html", null ]
    ] ],
    [ "Shared.IPin", "interface_shared_1_1_i_pin.html", [
      [ "Shared.IPinGeneric< T >", "interface_shared_1_1_i_pin_generic.html", [
        [ "Components.Components.Pin< T >", "class_components_1_1_components_1_1_pin.html", null ],
        [ "ComponentTest.Model.Component.Pin< T >", "class_component_test_1_1_model_1_1_component_1_1_pin.html", null ]
      ] ]
    ] ],
    [ "ISerializable", null, [
      [ "YALS_WaspEdition.GlobalConfig.SerializableColor", "class_y_a_l_s___wasp_edition_1_1_global_config_1_1_serializable_color.html", null ],
      [ "YALS_WaspEdition.ViewModels.NodeVM", "class_y_a_l_s___wasp_edition_1_1_view_models_1_1_node_v_m.html", null ]
    ] ],
    [ "Shared.IValue", "interface_shared_1_1_i_value.html", [
      [ "Shared.IValueGeneric< T >", "interface_shared_1_1_i_value_generic.html", [
        [ "Components.Components.ValueGeneric< T >", "class_components_1_1_components_1_1_value_generic.html", null ],
        [ "ComponentTest.Model.Value.ValueGeneric< T >", "class_component_test_1_1_model_1_1_value_1_1_value_generic.html", null ],
        [ "YALS_WaspEdition.Model.Component.Value.ValueGeneric< T >", "class_y_a_l_s___wasp_edition_1_1_model_1_1_component_1_1_value_1_1_value_generic.html", null ]
      ] ]
    ] ],
    [ "IValueConverter", null, [
      [ "YALS_WaspEdition.Converters.BitmapConverter", "class_y_a_l_s___wasp_edition_1_1_converters_1_1_bitmap_converter.html", null ],
      [ "YALS_WaspEdition.Converters.Inverter", "class_y_a_l_s___wasp_edition_1_1_converters_1_1_inverter.html", null ],
      [ "YALS_WaspEdition.Converters.SerializableColorToSolidColorBrush", "class_y_a_l_s___wasp_edition_1_1_converters_1_1_serializable_color_to_solid_color_brush.html", null ]
    ] ],
    [ "YALS_WaspEdition.MyEventArgs.NotificationEventArgs", "class_y_a_l_s___wasp_edition_1_1_my_event_args_1_1_notification_event_args.html", null ],
    [ "ComponentTest.Program", "class_component_test_1_1_program.html", null ],
    [ "YALS_WaspEdition.DI.Provider", "class_y_a_l_s___wasp_edition_1_1_d_i_1_1_provider.html", null ],
    [ "SerializationBinder", null, [
      [ "YALS_WaspEdition.Model.Serialization.AssemblySerializationBinder", "class_y_a_l_s___wasp_edition_1_1_model_1_1_serialization_1_1_assembly_serialization_binder.html", null ]
    ] ],
    [ "UserControl", null, [
      [ "YALS_WaspEdition.GlobalConfig.ColorSettingsListBox", "class_y_a_l_s___wasp_edition_1_1_global_config_1_1_color_settings_list_box.html", null ],
      [ "YALS_WaspEdition.Views.UserControls.ComponentUC", "class_y_a_l_s___wasp_edition_1_1_views_1_1_user_controls_1_1_component_u_c.html", null ],
      [ "YALS_WaspEdition.Views.UserControls.DesignerComponentUC", "class_y_a_l_s___wasp_edition_1_1_views_1_1_user_controls_1_1_designer_component_u_c.html", null ],
      [ "YALS_WaspEdition.Views.UserControls.PlayerUC", "class_y_a_l_s___wasp_edition_1_1_views_1_1_user_controls_1_1_player_u_c.html", null ]
    ] ],
    [ "Window", null, [
      [ "YALS_WaspEdition.GlobalConfig.ColorConfigurationWindow", "class_y_a_l_s___wasp_edition_1_1_global_config_1_1_color_configuration_window.html", null ],
      [ "YALS_WaspEdition.GlobalConfig.ValueInputWindow", "class_y_a_l_s___wasp_edition_1_1_global_config_1_1_value_input_window.html", null ],
      [ "YALS_WaspEdition.MainWindow", "class_y_a_l_s___wasp_edition_1_1_main_window.html", null ]
    ] ]
];